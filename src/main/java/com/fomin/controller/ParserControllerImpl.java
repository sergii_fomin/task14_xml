package com.fomin.controller;

import com.fomin.model.Flower;
import com.fomin.service.Gallery;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;

public class ParserControllerImpl implements ParserController {
    @Override
    public void printParsedXML() {
        Gallery gallery = new Gallery();
        String filename = "d:/Epam/Practice/Fomin_Sergii/task14_xml/src/main/resources/gallery.xml";
        File xmlDocument = Paths.get(filename).toFile();
        SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
            SAXParser parser = factory.newSAXParser();
            parser.parse(xmlDocument, gallery);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
        List<Flower> result = gallery.getFlowers();
        result.forEach(System.out::println);
    }
}
