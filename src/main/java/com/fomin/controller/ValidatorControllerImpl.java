package com.fomin.controller;

import com.fomin.validator.XmlValidator;

public class ValidatorControllerImpl implements ValidatorController {
    @Override
    public void printValidationResult() {
        String xml = "d:/Epam/Practice/Fomin_Sergii/task14_xml/src/main/resources/gallery.xml";

        String xsd = "d:/Epam/Practice/Fomin_Sergii/task14_xml/src/main/resources/gallery.xsd";

        boolean result = XmlValidator.validateAgainstXsd(xml, xsd);
        if (result) {
            System.out.println("XML is correct");
        } else {
            System.out.println("XML is incorrect");
        }
    }
}
