package com.fomin.model;

public class VisualParameters {
    private String stemColor;

    private String leafColor;

    private String flowerSize;

    public String getStemColor() {
        return stemColor;
    }

    public void setStemColor(String stemColor) {
        this.stemColor = stemColor;
    }

    public String getLeafColor() {
        return leafColor;
    }

    public void setLeafColor(String leafColor) {
        this.leafColor = leafColor;
    }

    public String getFlowerSize() {
        return flowerSize;
    }

    public void setFlowerSize(String flowerSize) {
        this.flowerSize = flowerSize;
    }

    @Override
    public String toString() {
        return "VisualParameters{" +
                "stemColor='" + stemColor + '\'' +
                ", leafColor='" + leafColor + '\'' +
                ", flowerSize='" + flowerSize + '\'' +
                '}';
    }
}
