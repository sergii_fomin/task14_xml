package com.fomin;

import com.fomin.controller.ParserControllerImpl;
import com.fomin.controller.ValidatorControllerImpl;
import com.fomin.view.Menu;

public class Application {
    public static void main(String[] args) {
        new Menu(new ParserControllerImpl(), new ValidatorControllerImpl());
    }
}
