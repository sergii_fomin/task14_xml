package com.fomin.service;

import com.fomin.model.Flower;
import com.fomin.model.GrowingTips;
import com.fomin.model.VisualParameters;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class Gallery extends DefaultHandler {

    private Flower flower;
    private VisualParameters visualParameters;
    private GrowingTips growingTips;
    private List<Flower> flowers = new ArrayList<>();

    private boolean isSoil = false;
    private boolean isOrigin = false;
    private boolean isDivision = false;
    private boolean isStemColor = false;
    private boolean isLeafColor = false;
    private boolean isFlowerSize = false;
    private boolean isTemperature = false;
    private boolean isLightning = false;
    private boolean isWatering = false;

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equalsIgnoreCase("flower")) {
            flower = new Flower();
            flower.setName(attributes.getValue("name"));
        }
        if (qName.equalsIgnoreCase("visualParameters")) {
            visualParameters = new VisualParameters();
        }
        if (qName.equalsIgnoreCase("growingTips")) {
            growingTips = new GrowingTips();
        }
        switch (qName) {
            case "soil":
                isSoil = true;
                break;
            case "origin":
                isOrigin = true;
                break;
            case "stemColor":
                isStemColor = true;
                break;
            case "leafColor":
                isLeafColor = true;
                break;
            case "flowerSize":
                isFlowerSize = true;
                break;
            case "isTemperature":
                isTemperature = true;
                break;
            case "isLightning":
                isLightning = true;
                break;
            case "isWatering":
                isWatering = true;
                break;
            case "multiplying":
                isDivision = true;
                break;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (isSoil) {
            flower.setSoil(new String(ch, start, length));
            isSoil = false;
        }
        if (isOrigin) {
            flower.setOrigin(new String(ch, start, length));
            isOrigin = false;
        }
        if (isStemColor) {
            visualParameters.setStemColor(new String(ch, start, length));
            isStemColor = false;
        }
        if (isLeafColor) {
            visualParameters.setLeafColor(new String(ch, start, length));
            isLeafColor = false;
        }
        if (isFlowerSize) {
            visualParameters.setFlowerSize(new String(ch, start, length));
            isFlowerSize = false;
        }
        if (isLightning) {
            growingTips.setLightning(Boolean.parseBoolean(new String(ch, start, length)));
            isLightning = false;
        }
        if (isTemperature) {
            growingTips.setTemperature(Integer.parseInt(new String(ch, start, length)));
            isTemperature = false;
        }
        if (isWatering) {
            growingTips.setWatering(Integer.parseInt(new String(ch, start, length)));
            isWatering = false;
        }
        if (isDivision) {
            flower.setDivision(new String(ch, start, length));
            isDivision = false;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        if (qName.equalsIgnoreCase("flower")) {
            flower.setGrowingTips(growingTips);
            flower.setVisualParameters(visualParameters);
            flowers.add(flower);
        }
    }

    public List<Flower> getFlowers() {
        return flowers;
    }

}
